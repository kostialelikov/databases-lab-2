declare module '@elastic.io/amqp-rpc'

/**
 *
 * @return {Promise<String>} queueName when server listens on for requests
 */
declare function initialSetup(): Promise<String>;

/**
 *
 * @param requestsQueue
 * @return {Promise<void>}
 */
declare function initServer(requestsQueue: any): Promise<void>;

/**
 *
 * @param requestsQueue
 * @return {Promise<void>}
 */
declare function initClient1(requestsQueue: any): Promise<void>;

/**
 *
 * @param requestsQueue
 * @return {Promise<void>}
 */
declare function initServer(requestsQueue: any): Promise<void>;

/**
 *
 * @param requestsQueue
 * @return {Promise<void>}
 */
declare function initClient1(requestsQueue: any): Promise<void>;



/**
 *
 * @param {*} connection Connection reference created from `amqplib` library
 *
 * @param {Object} [params]
 */
declare class AMQPEndpoint {
    constructor(connection: any, params?: any);
    /**
     * Initialization before starting working
     * NOTE! Race condition is not handled here,
     *    so it's better to not invoke the method several times (e.g. from multiple "threads")
     *
     * @return {Promise<void>}
     */
    start(): Promise<void>;
    /**
     * Opposite to this.start() – clearing
     * NOTE! Race condition is not handled here,
     *    so it's better to not invoke the method several times (e.g. from multiple "threads")
     *
     * @return {Promise<void>}
     */
    disconnect(): Promise<void>;
}

/**
 * @constructor
 * @param {amqplib.Connection} amqpConnection
 * @param {Object} params
 * @param {String} [params.queueName=''] queue for receiving events, should correspond with AMQPEventsSender
 *    default is '' which means auto-generated queue name, should correspond with AMQPEventsSender
 */
declare class AMQPEventsReceiver {
    
    constructor(amqpConnection: any, params: {
        queueName?: string;
    });
    /**
     * Begin to listen for messages from amqp
     * @returns {Promise<String>} name of endpoint to send messages
     * @override
     */
    start(): Promise<String>;
    /**
     * Stop listening for messages
     * @override
     */
    disconnect(): void;
    /**
     * Allows to get generated value when params.repliesQueue was set to '' (empty string) or omitted
     * @returns {String} an actual name of the queue used by the instance for receiving replies
     */
    queueName: any;
}

/**
 * @constructor
 * @param {amqplib.Connection} amqpConnection
 * @param {Object} params
 * @param {String} [params.queueName] queue for sending events, should correspond with AMQPEventsReceiver
 * @param {Number} [params.TTL=AMQPEventsSender.TTL] TTL of messages
 */
declare class AMQPEventsSender {
    constructor(amqpConnection: any, params: {
        queueName?: string;
        TTL?: number;
    });
    /**
     * Send message to receiver
     * @param {*} message, anything that may be serialized by JSON.stringify
     * @retiurns {Promise}
     */
    send(message: any): void;
    /**
     * Opposite to this.start() – closing communication channel
     * NOTE! Race condition is not handled here,
     *    so it's better to not invoke the method several times (e.g. from multiple "threads")
     *
     * @return {Promise<void>}
     */
    disconnect(): Promise<void>;
    /**
     * Channel initialization, has to be done before starting working
     * NOTE! Race condition is not handled here,
     *    so it's better to not invoke the method several times (e.g. from multiple "threads")
     *
     * @return {Promise<void>}
     */
    start(): Promise<void>;
    /**
     * Subscribe to events on channel.
     * Events used to understand, if listener is ok
     * and/or for error handling
     */
    _subscribeToChannel(): void;
    /**
     * Returns a timeout for a command result retrieval.
     *
     * @static
     * @returns {Number}
     */
    static TTL: any;
}

/**
 * Creates a new instance of an RPC client.
 *
 * @param {*} connection Instance of `amqplib` library
 *
 * @param {Object} params
 * @param {String} params.requestsQueue queue for sending commands, should correspond with AMQPRPCServer
 * @param {String} [params.repliesQueue=''] queue for feedback from AMQPRPCServer,
 *    default is '' which means auto-generated queue name
 * @param {Number} [params.timeout=60000] Timeout for cases when server is not responding
 * @param {Object} [params.defaultMessageOptions] additional options for publishing the request to the queue
 */
declare class AMQPRPCClient {
    constructor(connection: any, params: {
        requestsQueue: string;
        repliesQueue?: string;
        timeout?: number;
        defaultMessageOptions?: any;
    });
    /**
     * Send a command into RPC queue.
     *
     * @param {String} command Command name
     * @param args
     * @param [Object] messageOptions options for publishing the request to the queue
     * @returns {Promise<*>}
     * @example
     * client.sendCommand('some-command-name', [{foo: 'bar'}, [1, 2, 3]]);
     */
    sendCommand(command: string, args?: any, Object?: any): Promise<any>;
    /**
     * Initialize RPC client.
     *
     * @returns {Promise}
     * @override
     */
    start(): Promise<void>;
    /**
     * Opposite to this.start()
     *
     * @returns {Promise}
     */
    disconnect(): Promise<void>;
    /**
     * Returns a timeout for a command result retrieval.
     *
     * @static
     * @returns {Number}
     */
    static TIMEOUT: any;
    /**
     * Allows to get generated value when params.repliesQueue was set to '' (empty string) or omitted
     * @returns {String} an actual name of the queue used by the instance for receiving replies
     */
    repliesQueue: any;
}

/**
 * Creates a new instance of RPC server.
 *
 * @param {*} connection Connection reference created from `amqplib` library
 *
 * @param {Object} params
 * @param {String} params.requestsQueue queue when AMQPRPC client sends commands, should correspond with AMQPRPCClient
 *    default is '' which means auto-generated queue name
 */
declare class AMQPRPCServer {
    constructor(connection: any, params: {
        requestsQueue: string;
    });
    /**
     * Initialize RPC server.
     *
     * @returns {Promise}
     * @override
     */
    start(): Promise<void>;
    /**
     * Opposite to this.start()
     *
     * @returns {Promise}
     */
    disconnect(): Promise<void>;
    /**
     * Registers a new command in this RPC server instance.
     *
     * @param {String} command Command name
     * @param {Function} cb Callback that must be called when server got RPC command
     * @returns {AMQPRPCServer}
     */
    addCommand(command: string, cb: (...params: any[]) => any): AMQPRPCServer;
    /**
     * Allows to get generated value when params.requestsQueue was set to '' (empty string) or omitted
     * @returns {String} an actual name of the queue used by the instance for receiving replies
     */
    requestsQueue: any;
}

/**
 * Creates a new command instance.
 *
 * @param {String} command RPC command name
 * @param {Array<*>} args Array of arguments to provide an RPC
 * @example
 * const command = new Command('commandName', [
 *  {foo: 'bar'},
 *  [1, 2, 3]
 * ]);
 */
declare class Command {
    constructor(command: string, args: any[]);
    /**
     * Pack a command into the buffer for sending across queues.
     *
     * @returns {Buffer}
     */
    pack(): Buffer;
    /**
     * Static helper for creating new instances of a Command.
     *
     * @static
     * @param args
     * @returns {Command}
     */
    static create(...args: any[]): Command;
    /**
     * Static helper for creating new Command instances.
     *
     * @static
     * @param {Buffer} buffer
     * @returns {Command}
     */
    static fromBuffer(buffer: Buffer): Command;
}

/**
 * Creates a new instance of a command result.
 *
 * @param {String} state State from {@link CommandResult.STATES}
 * @param {*} data Any type that can be understandable by `JSON.stringify`
 * @example
 * const commandResult = new CommandResult({
 *  state: CommandResult.STATES.ERROR,
 *  data: new Error('Some error description'),
 * });
 *
 * const commandResult = new CommandResult({
 *  state: CommandResult.STATES.SUCCESS,
 *  data: ['some', 'data', 'here'],
 * });
 */
declare class CommandResult {
    constructor(state: string, data: any);
    /**
     * Packs a command result into the buffer for sending across queues.
     *
     * @returns {Buffer}
     */
    pack(): Buffer;
    /**
     * Returns a dictionary of possible STATES in the result.
     *
     * @static
     * @returns {{SUCCESS: String, ERROR: String}}
     */
    static STATES: any;
    /**
     * Static helper for creating new instances of {@link CommandResult}.
     *
     * @static
     * @param args
     * @returns {CommandResult}
     */
    static create(...args: any[]): CommandResult;
    /**
     * Static helper for creating a new instance of {@link CommandResult} from Buffer.
     *
     * @static
     * @param {Buffer} buffer
     * @returns {CommandResult}
     * @example
     * const commandResult = CommandResult.fromBuffer({state: CommandResult.STATES.SUCCESS, data: []});
     * const buffer = commandResult.pack();
     *
     * assert.instanceOf(buffer, Buffer);
     * assert.deepEqual(CommandResult.fromBuffer(buffer), commandResult);
     */
    static fromBuffer(buffer: Buffer): CommandResult;
}

/**
 * Get amqp connection
 * @returns {Promise<amqplib.Connection>}
 */
declare function getAmqpConnection(): Promise<any>;

/**
 * Close currently opened amqp connection
 */
declare function closeAmqpConnection(): void;

/**
 * Helper method, returns promise, that fulfills
 * when event is emitted on eventEmitter;
 * @param {String} event
 * @param {EventEmitter} eventEmitter
 * @returns {Promise}
 */
declare function waitFor(event: string, eventEmitter: NodeJS.EventEmitter): Promise<any>;

