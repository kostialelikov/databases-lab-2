export const db = {
    HOST: 'localhost',
    PORT: 5432,
    NAME: 'integrated_chat',
    USER: 'postgres',
    PASSWORD: 'postgres'
};

export const api = {
    PORT: 3000
};

export const rabbit = {
    HOST: 'localhost',
    PORT: 5672
};

export const rpc = {
    DB: 'db'
};

const config = {
    db,
    api,
    rabbit,
    rpc
};

export default config;
