import * as inquirer from 'inquirer';
import axios from 'axios';
import table from '../../helpers/table';
import chalk from 'chalk';
import faker from 'faker';
import {getPublicUrl} from "../../helpers/ngrok";

export enum ChatsModes {
    CREATE = 'Create new',
    FAKE = 'Create random',
    BACK = '<-',
    GET_ALL = 'Get all',
    GET_BY_ID = 'Get chat by id',
    REMOVE_BY_ID = 'Remove chat by id',
}

export const chatsPrompts = {
    menu: [
        {
            name: 'mode',
            type: 'list',
            message: "What's next?",
            choices: [
                ChatsModes.CREATE,
                ChatsModes.FAKE,
                ChatsModes.GET_ALL,
                ChatsModes.GET_BY_ID,
                ChatsModes.REMOVE_BY_ID,
                ChatsModes.BACK,
            ],
            default: 0,
        }
    ],
    create: [
        {
            name: 'name',
            type: 'input',
            message: 'Name',
            default: 'name',
        },
        {
            name: 'description',
            type: 'input',
            message: 'Description',
            default: 'description',
        }
    ],
    getAll: [
        {
            name: 'limit',
            type: 'number',
            message: 'Limit',
            default: 7,
            validate: (input: number) => {
                return input >= 0;
            }
        },
        {
            name: 'offset',
            type: 'number',
            message: 'Offset',
            default: 0,
            validate: (input: number) => {
                return input >= 0;
            }
        }
    ],
    getById: [
        {
            name: 'id',
            type: 'number',
            message: 'ID',
            validate: (input: number) => {
                return input > 0;
            }
        }
    ],
    removeById: [
        {
            name: 'id',
            type: 'number',
            message: 'ID',
            validate: (input: number) => {
                return input > 0;
            }
        }
    ]
};

export async function start() {
    while (true) {
        const answers: any = await inquirer.prompt(chatsPrompts.menu);
        switch (answers.mode) {
            case ChatsModes.GET_ALL: {
                await getAll();
                break;
            }
            case ChatsModes.CREATE: {
                await create();
                break;
            }
            case ChatsModes.FAKE: {
                await createFake();
                break;
            }
            case ChatsModes.GET_BY_ID: {
                await getById();
                break;
            }
            case ChatsModes.REMOVE_BY_ID: {
                await removeById();
                break;
            }
            case ChatsModes.BACK: {
                return;
            }
        }
    }
}

export async function create() {
    try {
        const answers: any = await inquirer.prompt(chatsPrompts.create);
        const response = await axios.post(`${await getPublicUrl(3000)}/api/chat`, answers);
        console.log(table.buildTable([response.data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function createFake() {
    try {
        const data = {
            name: faker.lorem.word(),
            description: faker.lorem.words(10)
        };
        const response = await axios.post(`${await getPublicUrl(3000)}/api/chat`, data);
        console.log(table.buildTable([response.data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getAll() {
    try {
        const answers: any = await inquirer.prompt(chatsPrompts.getAll);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/chat?limit=${answers.limit}&offset=${answers.offset}`);
        const data = response.data;
        console.log(table.buildTable(data));
        console.log(
            chalk.red('Limit: '),
            answers.limit,
            chalk.magenta('Page: '),
            answers.offset,
        );
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getById() {
    try {
        const answers: any = await inquirer.prompt(chatsPrompts.getById);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/chat/${answers.id}`);
        const data = response.data;
        console.log(table.buildTable([data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function removeById() {
    try {
        const answers: any = await inquirer.prompt(chatsPrompts.removeById);
        await axios.delete(`${await getPublicUrl(3000)}/api/chat/${answers.id}`);
        console.log(chalk.green('OK'));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}
