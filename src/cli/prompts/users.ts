import * as inquirer from 'inquirer';
import axios from 'axios';
import table from '../../helpers/table';
import chalk from 'chalk';
import faker from 'faker';
import {getPublicUrl} from "../../helpers/ngrok";

export enum UsersModes {
    CREATE = 'Create new',
    FAKE = 'Create random',
    BACK = '<-',
    GET_ALL = 'Get all',
    GET_BY_ID = 'Get user by id',
    REMOVE_BY_ID = 'Remove user by id',
}

export const usersPrompts = {
    menu: [
        {
            name: 'mode',
            type: 'list',
            message: "What's next?",
            choices: [
                UsersModes.CREATE,
                UsersModes.FAKE,
                UsersModes.GET_ALL,
                UsersModes.GET_BY_ID,
                UsersModes.REMOVE_BY_ID,
                UsersModes.BACK,
            ],
            default: 0,
        }
    ],
    create: [
        {
            name: 'username',
            type: 'input',
            message: 'Username:',
            default: 'username',
        },
        {
            name: 'firstName',
            type: 'input',
            message: 'First name:',
            default: 'first name',
        },
        {
            name: 'lastName',
            type: 'input',
            message: 'Last name:',
            default: 'last name',
        }
    ],
    getAll: [
        {
            name: 'limit',
            type: 'number',
            message: 'Limit',
            default: 7,
            validate: (input: number) => {
                return input >= 0;
            }
        },
        {
            name: 'offset',
            type: 'number',
            message: 'Offset',
            default: 0,
            validate: (input: number) => {
                return input >= 0;
            }
        },
        {
            name: 'search',
            type: 'input',
            message: 'Search string',
            default: ''
        },
    ],
    getById: [
        {
            name: 'id',
            type: 'number',
            message: 'ID',
            validate: (input: number) => {
                return input > 0;
            }
        }
    ],
    removeById: [
        {
            name: 'id',
            type: 'number',
            message: 'ID',
            validate: (input: number) => {
                return input > 0;
            }
        }
    ]
};

export async function start() {
    while (true) {
        const answers: any = await inquirer.prompt(usersPrompts.menu);
        switch (answers.mode) {
            case UsersModes.GET_ALL: {
                await getAll();
                break;
            }
            case UsersModes.CREATE: {
                await create();
                break;
            }
            case UsersModes.FAKE: {
                await createFake();
                break;
            }
            case UsersModes.GET_BY_ID: {
                await getById();
                break;
            }
            case UsersModes.REMOVE_BY_ID: {
                await removeById();
                break;
            }
            case UsersModes.BACK: {
                return;
            }
        }
    }
}

export async function create() {
    try {
        const answers: any = await inquirer.prompt(usersPrompts.create);
        const response = await axios.post(`${await getPublicUrl(3000)}/api/user`, answers);
        console.log(table.buildTable([response.data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function createFake() {
    try {
        const data = {
            username: faker.lorem.word(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName()
        };
        const response = await axios.post(`${await getPublicUrl(3000)}/api/user`, data);
        console.log(table.buildTable([response.data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getAll() {
    try {
        const answers: any = await inquirer.prompt(usersPrompts.getAll);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/user?limit=${answers.limit}&offset=${answers.offset}${answers.search ? `&search=${answers.search}` : ''}`);
        const data = response.data;
        console.log(table.buildTable(data));
        console.log(
            chalk.red('Limit: '),
            answers.limit,
            chalk.magenta('Page: '),
            answers.offset,
        );
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getById() {
    try {
        const answers: any = await inquirer.prompt(usersPrompts.getById);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/user/${answers.id}`);
        const data = response.data;
        console.log(table.buildTable([data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function removeById() {
    try {
        const answers: any = await inquirer.prompt(usersPrompts.removeById);
        await axios.delete(`${await getPublicUrl(3000)}/api/user/${answers.id}`);
        console.log(chalk.green('OK'));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}
