import * as inquirer from 'inquirer';
import axios from 'axios';
import table from '../../helpers/table';
import chalk from 'chalk';
import faker from 'faker';
import {getPublicUrl} from "../../helpers/ngrok";

export enum MessagesModes {
    CREATE = 'Create new',
    FAKE = 'Create random',
    BACK = '<-',
    GET_ALL = 'Get all',
    GET_BY_USER_ID = 'Get messages by user',
    GET_BY_ID = 'Get message by id',
    REMOVE_BY_ID = 'Remove message by id',
}

export const messagesPrompts = {
    menu: [
        {
            name: 'mode',
            type: 'list',
            message: "What's next?",
            choices: [
                MessagesModes.CREATE,
                MessagesModes.FAKE,
                MessagesModes.GET_ALL,
                MessagesModes.GET_BY_ID,
                MessagesModes.GET_BY_USER_ID,
                MessagesModes.REMOVE_BY_ID,
                MessagesModes.BACK,
            ],
            default: 0,
        }
    ],
    create: [
        {
            name: 'userId',
            type: 'number',
            message: 'User ID',
            default: 1,
            validate: (value: number) => {
                return value > 0;
            }
        },
        {
            name: 'message',
            type: 'input',
            message: 'Message',
            default: '',
        }
    ],
    getAll: [
        {
            name: 'limit',
            type: 'number',
            message: 'Limit',
            default: 7,
            validate: (input: number) => {
                return input >= 0;
            }
        },
        {
            name: 'offset',
            type: 'number',
            message: 'Offset',
            default: 0,
            validate: (input: number) => {
                return input >= 0;
            }
        }
    ],
    getById: [
        {
            name: 'id',
            type: 'number',
            message: 'ID',
            validate: (input: number) => {
                return input > 0;
            }
        }
    ],
    removeById: [
        {
            name: 'id',
            type: 'number',
            message: 'ID',
            validate: (input: number) => {
                return input > 0;
            }
        }
    ],
    getByUserId: [
        {
            name: 'userId',
            type: 'number',
            message: 'User ID',
            validate: (input: number) => {
                return input > 0;
            }
        },
        {
            name: 'limit',
            type: 'number',
            message: 'Limit',
            default: 7,
            validate: (input: number) => {
                return input >= 0;
            }
        },
        {
            name: 'offset',
            type: 'number',
            message: 'Offset',
            default: 0,
            validate: (input: number) => {
                return input >= 0;
            }
        }
    ]
};

export async function start() {
    while (true) {
        const answers: any = await inquirer.prompt(messagesPrompts.menu);
        switch (answers.mode) {
            case MessagesModes.GET_ALL: {
                await getAll();
                break;
            }
            case MessagesModes.CREATE: {
                await create();
                break;
            }
            case MessagesModes.FAKE: {
                await createFake();
                break;
            }
            case MessagesModes.GET_BY_ID: {
                await getById();
                break;
            }
            case MessagesModes.REMOVE_BY_ID: {
                await removeById();
                break;
            }
            case MessagesModes.GET_BY_USER_ID: {
                await getByUserId();
                break;
            }
            case MessagesModes.BACK: {
                return;
            }
        }
    }
}

export async function create() {
    try {
        const answers: any = await inquirer.prompt(messagesPrompts.create);
        const response = await axios.post(`${await getPublicUrl(3000)}/api/message`, answers);
        console.log(table.buildTable([response.data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function createFake() {
    try {
        const data = {
            userId: 18,
            message: faker.lorem.words(10),
        };
        const response = await axios.post(`${await getPublicUrl(3000)}/api/message`, data);
        console.log(table.buildTable([response.data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getAll() {
    try {
        const answers: any = await inquirer.prompt(messagesPrompts.getAll);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/message?limit=${answers.limit}&offset=${answers.offset}`);
        const data = response.data;
        console.log(table.buildTable(data));
        console.log(
            chalk.red('Limit: '),
            answers.limit,
            chalk.magenta('Page: '),
            answers.offset,
        );
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getByUserId() {
    try {
        const answers: any = await inquirer.prompt(messagesPrompts.getByUserId);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/message/user/${answers.userId}?limit=${answers.limit}&offset=${answers.offset}`);
        const data = response.data;
        console.log(table.buildTable(data));
        console.log(
            chalk.red('Limit: '),
            answers.limit,
            chalk.magenta('Page: '),
            answers.offset,
        );
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getById() {
    try {
        const answers: any = await inquirer.prompt(messagesPrompts.getById);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/message/${answers.id}`);
        const data = response.data;
        console.log(table.buildTable([data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function removeById() {
    try {
        const answers: any = await inquirer.prompt(messagesPrompts.removeById);
        await axios.delete(`${await getPublicUrl(3000)}/api/message/${answers.id}`);
        console.log(chalk.green('OK'));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}
