import * as inquirer from 'inquirer';
import axios from 'axios';
import table from '../../helpers/table';
import chalk from 'chalk';
import faker from 'faker';
import {getPublicUrl} from "../../helpers/ngrok";

export enum ChatsUsersModes {
    CREATE = 'Create new',
    BACK = '<-',
    GET_ALL = 'Get all',
    GET_ALL_BY_USER_ID = 'Get all by user ID',
    GET_ALL_BY_CHAT_ID = 'Get all by CHAT ID',
    GET_BY_ID = 'Get chat-user by id',
    REMOVE_BY_ID = 'Remove chat-user by id',
}

export const chatsUsersPrompts = {
    menu: [
        {
            name: 'mode',
            type: 'list',
            message: "What's next?",
            choices: [
                ChatsUsersModes.CREATE,
                ChatsUsersModes.GET_ALL,
                ChatsUsersModes.GET_ALL_BY_USER_ID,
                ChatsUsersModes.GET_ALL_BY_CHAT_ID,
                ChatsUsersModes.GET_BY_ID,
                ChatsUsersModes.REMOVE_BY_ID,
                ChatsUsersModes.BACK,
            ],
            default: 0,
        }
    ],
    create: [
        {
            name: 'userId',
            type: 'number',
            message: 'User ID',
            default: '1',
            validate: (value: number) => value > 0
        },
        {
            name: 'chatId',
            type: 'number',
            message: 'Chat ID',
            default: '1',
            validate: (value: number) => value > 0
        }
    ],
    getByChatId: [
        {
            name: 'chatId',
            type: 'number',
            message: 'Chat ID',
            default: '1',
            validate: (value: number) => value > 0
        },
        {
            name: 'limit',
            type: 'number',
            message: 'Limit',
            default: 7,
            validate: (input: number) => {
                return input >= 0;
            }
        },
        {
            name: 'offset',
            type: 'number',
            message: 'Offset',
            default: 0,
            validate: (input: number) => {
                return input >= 0;
            }
        }
    ],
    getByUserId: [
        {
            name: 'userId',
            type: 'number',
            message: 'User ID',
            default: '1',
            validate: (value: number) => value > 0
        },
        {
            name: 'limit',
            type: 'number',
            message: 'Limit',
            default: 7,
            validate: (input: number) => {
                return input >= 0;
            }
        },
        {
            name: 'offset',
            type: 'number',
            message: 'Offset',
            default: 0,
            validate: (input: number) => {
                return input >= 0;
            }
        }
    ],
    getAll: [
        {
            name: 'limit',
            type: 'number',
            message: 'Limit',
            default: 7,
            validate: (input: number) => {
                return input >= 0;
            }
        },
        {
            name: 'offset',
            type: 'number',
            message: 'Offset',
            default: 0,
            validate: (input: number) => {
                return input >= 0;
            }
        }
    ],
    getById: [
        {
            name: 'id',
            type: 'number',
            message: 'ID',
            validate: (input: number) => {
                return input > 0;
            }
        }
    ],
    removeById: [
        {
            name: 'id',
            type: 'number',
            message: 'ID',
            validate: (input: number) => {
                return input > 0;
            }
        }
    ]
};

export async function start() {
    while (true) {
        const answers: any = await inquirer.prompt(chatsUsersPrompts.menu);
        switch (answers.mode) {
            case ChatsUsersModes.GET_ALL: {
                await getAll();
                break;
            }
            case ChatsUsersModes.CREATE: {
                await create();
                break;
            }
            case ChatsUsersModes.GET_BY_ID: {
                await getById();
                break;
            }
            case ChatsUsersModes.GET_ALL_BY_USER_ID: {
                await getByUserId();
                break;
            }
            case ChatsUsersModes.GET_ALL_BY_CHAT_ID: {
                await getByChatId();
                break;
            }
            case ChatsUsersModes.REMOVE_BY_ID: {
                await removeById();
                break;
            }
            case ChatsUsersModes.BACK: {
                return;
            }
        }
    }
}

export async function create() {
    try {
        const answers: any = await inquirer.prompt(chatsUsersPrompts.create);
        const response = await axios.post(  `${await getPublicUrl(3000)}/api/chatUser`, answers);
        console.log(table.buildTable([response.data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getAll() {
    try {
        const answers: any = await inquirer.prompt(chatsUsersPrompts.getAll);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/chatUser?limit=${answers.limit}&offset=${answers.offset}`);
        const data = response.data;
        console.log(table.buildTable(data));
        console.log(
            chalk.red('Limit: '),
            answers.limit,
            chalk.magenta('Page: '),
            answers.offset,
        );
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getByUserId() {
    try {
        const answers: any = await inquirer.prompt(chatsUsersPrompts.getByUserId);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/chatUser/user/${answers.userId}?limit=${answers.limit}&offset=${answers.offset}`);
        const data = response.data;
        console.log(table.buildTable(data));
        console.log(
            chalk.red('Limit: '),
            answers.limit,
            chalk.magenta('Page: '),
            answers.offset,
        );
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getByChatId() {
    try {
        const answers: any = await inquirer.prompt(chatsUsersPrompts.getByChatId);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/chatUser/chat/${answers.chatId}?limit=${answers.limit}&offset=${answers.offset}`);
        const data = response.data;
        console.log(table.buildTable(data));
        console.log(
            chalk.red('Limit: '),
            answers.limit,
            chalk.magenta('Page: '),
            answers.offset,
        );
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function getById() {
    try {
        const answers: any = await inquirer.prompt(chatsUsersPrompts.getById);
        const response = await axios.get(`${await getPublicUrl(3000)}/api/chatUser/${answers.id}`);
        const data = response.data;
        console.log(table.buildTable([data]));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}

export async function removeById() {
    try {
        const answers: any = await inquirer.prompt(chatsUsersPrompts.removeById);
        await axios.delete(`${await getPublicUrl(3000)}/api/chatUser/${answers.id}`);
        console.log(chalk.green('OK'));
    } catch (e) {
        console.log(chalk.red((e.response && e.response.data) || e.message));
    }
}
