import clear from 'clear';
import * as inquirer from 'inquirer';
import {start as usersMain} from './users';
import {start as messagesMain} from './messages';
import {start as chatsMain} from './chats';
import {start as chatsUsersMain} from './chatsUsers';

export enum MenuModes {
    USERS_SECTION = 'Manage users',
    CHATS_SECTION = 'Manage chats',
    MESSAGES_SECTION = 'Manage messages',
    CHATS_USERS_SECTION = 'Manage chats-users',
    EXIT = 'Exit'
}

export const menuPrompts = {
    menu: [{
        name: 'section',
        type: 'list',
        message: 'Welcome choose what to do.',
        choices: [
            MenuModes.USERS_SECTION,
            MenuModes.CHATS_SECTION,
            MenuModes.MESSAGES_SECTION,
            MenuModes.CHATS_USERS_SECTION,
            MenuModes.EXIT
        ],
        default: MenuModes.USERS_SECTION
    }]
};

export async function start() {
    while(true) {
        clear();
        const answer: any = await inquirer.prompt(menuPrompts.menu);
        switch (answer.section) {
            case MenuModes.USERS_SECTION: {
                await usersMain();
                break;
            }
            case MenuModes.MESSAGES_SECTION: {
                await messagesMain();
                break;
            }
            case MenuModes.CHATS_SECTION: {
                await chatsMain();
                break;
            }
            case MenuModes.CHATS_USERS_SECTION: {
                await chatsUsersMain();
                break;
            }
            case MenuModes.EXIT: {
                return;
            }
        }
    }
}
