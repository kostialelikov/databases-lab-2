import {RPCServer, IConfigurationServer} from '../../helpers/rpc-server';
import {database} from './database';

export class Server extends RPCServer implements IConfigurationServer {
    public async configureServer() {
        await this.initialSetup();

        // User section
        this.server.addCommand('users:create', async () =>
            await database.users.create());
        this.server.addCommand('users:drop', async () =>
            await database.users.drop());
        this.server.addCommand('users:addIfNotExists', async (username: string, firstName: string, lastName: string) =>
            await database.users.addIfNotExists(username, firstName, lastName));
        this.server.addCommand('users:removeById', async (id: number) =>
            await database.users.removeById(id));
        this.server.addCommand('users:updateById', async (id: number, username: string, firstName: string, lastName: string) =>
            await database.users.updateById(id, username, firstName, lastName));
        this.server.addCommand('users:getById', async (id: number) =>
            await database.users.getById(id));
        this.server.addCommand('users:getByUsername', async (username: string) =>
            await database.users.getByUsername(username));
        this.server.addCommand('users:getAll', async (limit: number, offset: number, search: string = '') =>
            await database.users.getAll(limit, offset, search));
        this.server.addCommand('users:count', async () =>
            await database.users.count());

        // Message section
        this.server.addCommand('messages:create', async () =>
            await database.messages.create());
        this.server.addCommand('messages:drop', async () =>
            await database.messages.drop());
        this.server.addCommand('messages:add', async (userId: number, message: string) =>
            await database.messages.add(userId, message));
        this.server.addCommand('messages:removeById', async (id: number) =>
            await database.messages.removeById(id));
        this.server.addCommand('messages:getAll', async (limit: number, offset: number) =>
            await database.messages.getAll(limit, offset));
        this.server.addCommand('messages:getAllByUserId', async (userId: number, page: number, pageSize: number = 7) =>
            await database.messages.getAllByUserId(userId, page, pageSize));
        this.server.addCommand('messages:getById', async (id: number) =>
            await database.messages.getById(id));

        // Chat section
        this.server.addCommand('chats:create', async () =>
            await database.chats.create());
        this.server.addCommand('chats:drop', async () =>
            await database.chats.drop());
        this.server.addCommand('chats:add', async (name: string, description: string) =>
            await database.chats.add(name, description));
        this.server.addCommand('chats:removeById', async (id: number) =>
            await database.chats.removeById(id));
        this.server.addCommand('chats:getAll', async (limit: number, offset: number) =>
            await database.chats.getAll(limit, offset));
        this.server.addCommand('chats:getById', async (id: number) =>
            await database.chats.getById(id));


        //Chats Users section
        this.server.addCommand('chats_users:create', async () =>
            await database.chatsUsers.create());
        this.server.addCommand('chats_users:drop', async () =>
            await database.chatsUsers.drop());
        this.server.addCommand('chats_users:add', async (userId: number, chatId: number) =>
            await database.chatsUsers.add(userId, chatId));
        this.server.addCommand('chats_users:removeById', async (id: number) =>
            await database.chatsUsers.removeById(id));
        this.server.addCommand('chats_users:getAll', async (limit: number, offset: number) =>
            await database.chatsUsers.getAll(limit, offset));
        this.server.addCommand('chats_users:getById', async (id: number) =>
            await database.chatsUsers.getById(id));
        this.server.addCommand('chats_users:getByUserId', async (id: number, limit: number, offset: number) =>
            await database.chatsUsers.getByUserId(id, limit, offset));
        this.server.addCommand('chats_users:getByChatId', async (id: number, limit: number, offset: number) =>
            await database.chatsUsers.getByChatId(id, limit, offset));
        this.server.addCommand('chats_users:removeByUserId', async (id: number) =>
            await database.chatsUsers.removeUserId(id));
        this.server.addCommand('chats_users:removeByChatId', async (id: number) =>
            await database.chatsUsers.removeChatId(id));


        return this;
    }
}
