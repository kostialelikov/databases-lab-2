import pgPromise from 'pg-promise';
import {db} from '../../../config';
import {camelizeColumnsNames} from '../../helpers/camelize-colimns-names';
import * as DBModels from './models';

export interface IDatabase extends pgPromise.IDatabase<DBModels.IDBModels>, DBModels.IDBModels {
}

const connectionOptions = {
    host: db.HOST,
    port: db.PORT,
    database: db.NAME,
    user: db.USER,
    password: db.PASSWORD,
};

const initOptions: pgPromise.IInitOptions<DBModels.IDBModels> = {
    extend(obj: IDatabase) {
        obj.users = new DBModels.UserModel(obj, pgp);
        obj.messages = new DBModels.MessageModel(obj, pgp);
        obj.chats = new DBModels.ChatModel(obj, pgp);
        obj.chatsUsers = new DBModels.ChatsUsersModel(obj, pgp);
    },
    receive: camelizeColumnsNames,
    query: e => console.log(e)
};

const pgp = pgPromise(initOptions);

const database = pgp(connectionOptions);

database.connect()
    .then((obj) => {
        /// @TODO redirect log
        return obj.done();
    })
    .then(() => {
        return DBModels.init(database);
    })
    .catch(err => {
        console.error(err)
        /// @TODO redirect error
    });

export {
    database
}
