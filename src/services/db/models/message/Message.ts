import {IMain} from 'pg-promise';
import {IDatabase} from '../../database';
import sql, {IMessageQueryTree} from './sql';

export interface IMessageRecord {
    id: number;
    userId: number;
    message: string;
}

export class MessageModel {
    protected database: IDatabase;
    protected pgp: IMain;
    protected sql: IMessageQueryTree = sql;

    constructor(database: IDatabase, pgp: IMain) {
        this.database = database;
        this.pgp = pgp;
    }

    create(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.create);
                resolve();
            } catch (err) {
                reject(err);
            }
        })
    }

    drop(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.drop);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    add(userId: number, message: string): Promise<IMessageRecord> {
        return new Promise<IMessageRecord>(async (resolve, reject) => {
            try {
                const messageRecord = await this.database.one(this.sql.add, {
                    userId,
                    message
                });
                resolve(messageRecord);
            } catch (err) {
                reject(err);
            }
        });
    }

    removeById(id: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.removeById, {
                    id
                });
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    getById(id: number): Promise<IMessageRecord> {
        return new Promise<IMessageRecord>(async (resolve, reject) => {
            try {
                const message = await this.database.one(this.sql.getById, {
                    id
                });
                resolve(message);
            } catch (err) {
                reject(err);
            }
        });
    }

    getAll(limit: number, offset: number): Promise<IMessageRecord[]> {
        return new Promise<IMessageRecord[]>(async (resolve, reject) => {
            try {
                const messages = await this.database.many(this.sql.getAll, {
                    limit,
                    offset
                });
                resolve(messages);
            } catch (err) {
                reject(err);
            }
        })
    }

    getAllByUserId(userId: number, limit: number, offset: number): Promise<IMessageRecord[]> {
        return new Promise<IMessageRecord[]>(async (resolve, reject) => {
            try {
                const messages = await this.database.many(this.sql.getAllByUserId, {
                    userId,
                    limit,
                    offset
                });
                resolve(messages);
            } catch (err) {
                reject(err);
            }
        });
    }
}
