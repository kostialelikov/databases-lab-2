create table if not exists messages
(
    id      serial primary key,
    user_id integer not null references users (id) on delete cascade,
    message text    not null
)
