import {load} from '../../../../../helpers/sql-loader';
import {QueryFile} from "pg-promise";

export interface IMessageQueryTree {
    create: QueryFile
    drop: QueryFile
    getAll: QueryFile
    getAllByUserId: QueryFile
    getById: QueryFile
    removeById: QueryFile
    add: QueryFile
}

const tree: IMessageQueryTree = load(process.cwd() + '/src/services/db/models/message/sql');

export default tree;
