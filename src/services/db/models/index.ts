import {UserModel} from './user/User';
import {MessageModel} from "./message/Message";
import {ChatModel} from "./chat/Chat";
import {ChatsUsersModel} from "./chats_users/ChatsUsers";
import {IDatabase} from '../database';

export interface IDBModels {
    users: UserModel
    messages: MessageModel,
    chats: ChatModel,
    chatsUsers: ChatsUsersModel
}

export {
    UserModel,
    MessageModel,
    ChatModel,
    ChatsUsersModel
}

export async function init(db: IDatabase) {
    await db.users.create();
    await db.messages.create();
    await db.chats.create();
    await db.chatsUsers.create();
}
