import {IMain} from 'pg-promise';
import {IDatabase} from '../../database';
import sql, {IUserQueryTree} from './sql';

export interface IUserRecord {
    id: number;
    username: string;
    firstName?: string;
    lastName?: string;
}

export class UserModel {
    protected database: IDatabase;
    protected pgp: IMain;
    protected sql: IUserQueryTree = sql;

    constructor(database: IDatabase, pgp: IMain) {
        this.database = database;
        this.pgp = pgp;
    }

    create(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.create);
                resolve();
            } catch (err) {
                reject(err);
            }
        })
    }

    drop(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.drop);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    addIfNotExists(username: string, firstName: string, lastName: string): Promise<IUserRecord> {
        return new Promise<IUserRecord>(async (resolve, reject) => {
            try {
                const user = await this.database.one(this.sql.addIfNotExists, {
                    username,
                    firstName,
                    lastName
                });
                resolve(user);
            } catch (err) {
                reject(err);
            }
        });
    }

    removeById(id: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.removeById, {
                    id
                });
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    updateById(id: number, username: string, firstName: string, lastName: string): Promise<IUserRecord> {
        return new Promise<IUserRecord>(async (resolve, reject) => {
            try {
                const user = await this.database.oneOrNone(this.sql.updateById, {
                    id,
                    username,
                    firstName,
                    lastName
                });
                resolve(user);
            } catch (err) {
                reject(err);
            }
        })
    }

    getById(id: number): Promise<IUserRecord> {
        return new Promise<IUserRecord>(async (resolve, reject) => {
            try {
                const user = await this.database.oneOrNone(this.sql.getById, {
                    id
                });
                resolve(user);
            } catch (err) {
                reject(err);
            }
        });
    }

    getByUsername(username: string): Promise<IUserRecord> {
        return new Promise<IUserRecord>(async (resolve, reject) => {
            try {
                const user = await this.database.oneOrNone(this.sql.getByUsername, {
                    username
                });
                resolve(user);
            } catch (err) {
                reject(err);
            }
        });
    }

    getAll(limit: number, offset: number, search: string = ''): Promise<IUserRecord[]> {
        return new Promise<IUserRecord[]>(async (resolve, reject) => {
            try {
                const users = await this.database.manyOrNone(this.sql.getAll, {
                    limit,
                    offset,
                    search
                });
                resolve(users);
            } catch (err) {
                reject(err);
            }
        })
    }

    count(): Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            try {
                const count = await this.database.one(this.sql.count);
                resolve(count);
            } catch (err) {
                reject(err);
            }
        });
    }
}
