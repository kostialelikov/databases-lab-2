INSERT INTO users (username, first_name, last_name)
VALUES (${username}, ${firstName}, ${lastName})
ON CONFLICT (username) DO NOTHING
RETURNING *;
