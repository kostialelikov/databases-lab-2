SELECT *
FROM users
WHERE username LIKE '%${search:raw}%'
   OR first_name LIKE '%${search:raw}%'
   OR last_name LIKE '%${search:raw}%'
LIMIT ${limit}
OFFSET ${offset};
