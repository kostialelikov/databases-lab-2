UPDATE users
SET username   = ${username},
    first_name = ${firstName},
    last_name  = ${lastName}
WHERE id = ${id}
RETURNING *;
