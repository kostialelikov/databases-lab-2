import {load} from '../../../../../helpers/sql-loader';
import {QueryFile} from "pg-promise";

export interface IUserQueryTree {
    create: QueryFile
    drop: QueryFile
    addIfNotExists: QueryFile;
    removeById: QueryFile;
    updateById: QueryFile;
    getById: QueryFile;
    getByUsername: QueryFile;
    getAll: QueryFile;
    count: QueryFile;
}

const tree: IUserQueryTree = load(process.cwd() + '/src/services/db/models/user/sql');

export default tree;
