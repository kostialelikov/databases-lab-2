create table if not exists users
(
    id         serial primary key,
    username   varchar(255) unique not null,
    first_name varchar(255),
    last_name  varchar(255)
)
