import {IMain} from 'pg-promise';
import {IDatabase} from '../../database';
import sql, {IChatsQueryTree} from './sql';

export interface IChatRecord {
    id: number;
    name: string;
    description: string;
}

export class ChatModel {
    protected database: IDatabase;
    protected pgp: IMain;
    protected sql: IChatsQueryTree = sql;

    constructor(database: IDatabase, pgp: IMain) {
        this.database = database;
        this.pgp = pgp;
    }

    create(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.create);
                resolve();
            } catch (err) {
                reject(err);
            }
        })
    }

    drop(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.drop);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    add(name: string, description: string): Promise<IChatRecord> {
        return new Promise<IChatRecord>(async (resolve, reject) => {
            try {
                const chatRecord = await this.database.one(this.sql.add, {
                    name,
                    description
                });
                resolve(chatRecord);
            } catch (err) {
                reject(err);
            }
        });
    }

    removeById(id: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.removeById, {
                    id
                });
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    getById(id: number): Promise<IChatRecord> {
        return new Promise<IChatRecord>(async (resolve, reject) => {
            try {
                const chat = await this.database.one(this.sql.getById, {
                    id
                });
                resolve(chat);
            } catch (err) {
                reject(err);
            }
        });
    }

    getAll(limit: number, offset: number): Promise<IChatRecord[]> {
        return new Promise<IChatRecord[]>(async (resolve, reject) => {
            try {
                const chats = await this.database.many(this.sql.getAll, {
                    limit,
                    offset
                });
                resolve(chats);
            } catch (err) {
                reject(err);
            }
        })
    }
}
