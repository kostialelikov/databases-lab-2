import {load} from '../../../../../helpers/sql-loader';
import {QueryFile} from "pg-promise";

export interface IChatsQueryTree {
    create: QueryFile
    drop: QueryFile
    getAll: QueryFile
    getById: QueryFile
    removeById: QueryFile
    add: QueryFile
}

const tree: IChatsQueryTree = load(process.cwd() + '/src/services/db/models/chat/sql');

export default tree;
