create table if not exists chats
(
    id          serial primary key,
    name        varchar not null,
    description text    not null
);
