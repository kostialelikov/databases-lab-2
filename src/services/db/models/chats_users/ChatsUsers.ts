import {IMain} from 'pg-promise';
import {IDatabase} from '../../database';
import sql, {IChatsUsersQueryTree} from './sql';

export interface IChatsUsersRecord {
    id: number;
    chat_id: number;
    user_id: number;
}

export class ChatsUsersModel {
    protected database: IDatabase;
    protected pgp: IMain;
    protected sql: IChatsUsersQueryTree = sql;

    constructor(database: IDatabase, pgp: IMain) {
        this.database = database;
        this.pgp = pgp;
    }

    create(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.create);
                resolve();
            } catch (err) {
                reject(err);
            }
        })
    }

    drop(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.drop);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    add(userId: number, chatId: number): Promise<IChatsUsersRecord> {
        return new Promise<IChatsUsersRecord>(async (resolve, reject) => {
            try {
                const chatsUsersRecord = await this.database.one(this.sql.add, {
                    userId,
                    chatId
                });
                resolve(chatsUsersRecord);
            } catch (err) {
                reject(err);
            }
        });
    }

    removeById(id: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.removeById, {
                    id
                });
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    removeUserId(userId: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.removeByUserId, {
                    userId
                });
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    removeChatId(chatId: number): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                await this.database.none(this.sql.removeByChatId, {
                    chatId
                });
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    getById(id: number): Promise<IChatsUsersRecord> {
        return new Promise<IChatsUsersRecord>(async (resolve, reject) => {
            try {
                const chatUser = await this.database.one(this.sql.getById, {
                    id
                });
                resolve(chatUser);
            } catch (err) {
                reject(err);
            }
        });
    }

    getByUserId(userId: number, limit: number, offset: number): Promise<IChatsUsersRecord[]> {
        return new Promise<IChatsUsersRecord[]>(async (resolve, reject) => {
            try {
                const chatsUsers = await this.database.many(this.sql.getByUserId, {
                    userId,
                    limit,
                    offset
                });
                resolve(chatsUsers);
            } catch (err) {
                reject(err);
            }
        });
    }

    getByChatId(chatId: number, limit: number, offset: number): Promise<IChatsUsersRecord[]> {
        return new Promise<IChatsUsersRecord[]>(async (resolve, reject) => {
            try {
                const chatsUsers = await this.database.many(this.sql.getByChatId, {
                    chatId,
                    limit,
                    offset
                })
                resolve(chatsUsers);
            } catch (err) {
                reject(err);
            }
        });
    }

    getAll(limit: number, offset: number): Promise<IChatsUsersRecord[]> {
        return new Promise<IChatsUsersRecord[]>(async (resolve, reject) => {
            try {
                const chats = await this.database.many(this.sql.getAll, {
                    limit,
                    offset
                });
                resolve(chats);
            } catch (err) {
                reject(err);
            }
        })
    }
}
