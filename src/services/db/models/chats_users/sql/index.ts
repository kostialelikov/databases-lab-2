import {load} from '../../../../../helpers/sql-loader';
import {QueryFile} from "pg-promise";

export interface IChatsUsersQueryTree {
    create: QueryFile
    drop: QueryFile
    getAll: QueryFile
    getById: QueryFile
    getByUserId: QueryFile
    getByChatId: QueryFile
    removeById: QueryFile
    removeByUserId: QueryFile
    removeByChatId: QueryFile
    add: QueryFile
}

const tree: IChatsUsersQueryTree = load(process.cwd() + '/src/services/db/models/chats_users/sql');

export default tree;
