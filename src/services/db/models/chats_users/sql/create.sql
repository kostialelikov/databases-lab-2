create table if not exists chats_users
(
    id      serial primary key,
    chat_id integer not null references chats (id) on delete cascade,
    user_id integer not null references users (id) on delete cascade
);
