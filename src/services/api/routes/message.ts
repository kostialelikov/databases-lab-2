import {Router} from 'express';
import {DBRPC} from '../index';
import httpStatus from 'http-status';

const router = Router();

router.post('/', async (req, res) => {
    const {userId, message} = req.body;
    try {
        const msg = await DBRPC.sendCommand('messages:add', [
            userId,
            message
        ]);
        res.status(httpStatus.OK).send(msg);
    } catch (err) {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send(err.message);
    }
});

router.delete('/:messageId', async (req, res) => {
    const {messageId} = req.params;
    try {
        await DBRPC.sendCommand('messages:removeById', [messageId]);
        res.sendStatus(httpStatus.OK);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/:messageId', async (req, res) => {
    const {messageId} = req.params;
    try {
        const message = await DBRPC.sendCommand('messages:getById', [messageId]);
        res.status(httpStatus.OK).send(message);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/', async (req, res) => {
    const {limit, offset} = req.query;
    try {
        const messages = await DBRPC.sendCommand('messages:getAll', [
            limit,
            offset
        ]);
        res.status(httpStatus.OK).send(messages);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/user/:userId', async (req, res) => {
    const {userId} = req.params;
    const {limit, offset} = req.query;
    try {
        const messages = await DBRPC.sendCommand('messages:getAllByUserId', [
            userId,
            limit,
            offset
        ]);
        res.status(httpStatus.OK).send(messages);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

export default router;
