import {Router} from 'express';
import {DBRPC} from '../index';
import httpStatus from 'http-status';

const router = Router();

router.post('/', async (req, res) => {
    const {userId, chatId} = req.body;
    try {
        const chatUser = await DBRPC.sendCommand('chats_users:add', [
            userId,
            chatId
        ]);
        res.status(httpStatus.OK).send(chatUser);
    } catch (err) {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send(err.message);
    }
});

router.delete('/:chatUserId', async (req, res) => {
    const {chatUserId} = req.params;
    try {
        await DBRPC.sendCommand('chats_users:removeById', [chatUserId]);
        res.sendStatus(httpStatus.OK);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/:chatUserId', async (req, res) => {
    const {chatUserId} = req.params;
    try {
        const chat = await DBRPC.sendCommand('chats_users:getById', [chatUserId]);
        res.status(httpStatus.OK).send(chat);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/', async (req, res) => {
    const {limit, offset} = req.query;
    try {
        const chatsUsers = await DBRPC.sendCommand('chats_users:getAll', [
            limit,
            offset,
        ]);
        res.status(httpStatus.OK).send(chatsUsers);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/user/:userId', async (req, res) => {
    const {userId} = req.params;
    const {limit, offset} = req.query;
    try {
        const chatsUsers = await DBRPC.sendCommand('chats_users:getByUserId', [
            userId,
            limit,
            offset,
        ]);
        res.status(httpStatus.OK).send(chatsUsers);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/chat/:chatId', async (req, res) => {
    const {chatId} = req.params;
    const {limit, offset} = req.query;
    try {
        const chatsUsers = await DBRPC.sendCommand('chats_users:getByChatId', [
            chatId,
            limit,
            offset,
        ]);
        res.status(httpStatus.OK).send(chatsUsers);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.delete('/user/:userId', async (req, res) => {
    const {userId} = req.params;
    try {
        await DBRPC.sendCommand('chats_users:removeByUserId', [userId]);
        res.sendStatus(httpStatus.OK);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.delete('/chat/:chatId', async (req, res) => {
    const {chatId} = req.params;
    try {
        await DBRPC.sendCommand('chats_users:removeByChatId', [chatId]);
        res.sendStatus(httpStatus.OK);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

export default router;
