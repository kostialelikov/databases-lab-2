import {Router} from 'express';
import {DBRPC} from '../index';
import httpStatus from 'http-status';

const router = Router();

router.post('/', async (req, res) => {
    const {username, firstName, lastName} = req.body;
    try {
        const user = await DBRPC.sendCommand('users:addIfNotExists', [
            username,
            firstName,
            lastName
        ]);
        res.status(httpStatus.OK).send(user);
    } catch (err) {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send(err.message);
    }
});

router.delete('/:userId', async (req, res) => {
    const {userId} = req.params;
    try {
        await DBRPC.sendCommand('users:removeById', [userId]);
        res.sendStatus(httpStatus.OK);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.put('/:userId', async (req, res) => {
    const {userId} = req.params;
    const {username, firstName, lastName} = req.body;
    try {
        await DBRPC.sendCommand('users:updateById', [
            userId,
            username,
            firstName,
            lastName
        ]);
        res.sendStatus(httpStatus.OK);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/:userId', async (req, res) => {
    const {userId} = req.params;
    try {
        const user = await DBRPC.sendCommand('users:getById', [userId]);
        res.status(httpStatus.OK).send(user);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/', async (req, res) => {
    const {limit, search, offset} = req.query;
    try {
        const users = await DBRPC.sendCommand('users:getAll', [
            limit,
            offset,
            search ? search : '',
        ]);
        res.status(httpStatus.OK).send(users);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/username/:username', async (req, res) => {
    const {username} = req.params;
    try {
        const user = await DBRPC.sendCommand('users:getByUsername', [
            username
        ]);
        res.status(httpStatus.OK).send(user);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

export default router;
