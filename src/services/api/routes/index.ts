import {Router} from 'express';
import user from './user';
import message from './message';
import chat from './chat';
import chatUser from './chatUser';

const router = Router();

router.use('/user', user);
router.use('/message', message);
router.use('/chat', chat);
router.use('/chatUser', chatUser);

export default router;
