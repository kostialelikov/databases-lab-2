import {Router} from 'express';
import {DBRPC} from '../index';
import httpStatus from 'http-status';

const router = Router();

router.post('/', async (req, res) => {
    const {name, description} = req.body;
    try {
        const chat = await DBRPC.sendCommand('chats:add', [
            name,
            description
        ]);
        res.status(httpStatus.OK).send(chat);
    } catch (err) {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).send(err.message);
    }
});

router.delete('/:chatId', async (req, res) => {
    const {chatId} = req.params;
    try {
        await DBRPC.sendCommand('chats:removeById', [chatId]);
        res.sendStatus(httpStatus.OK);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
})

router.get('/:chatId', async (req, res) => {
    const {chatId} = req.params;
    try {
        const chat = await DBRPC.sendCommand('chats:getById', [chatId]);
        res.status(httpStatus.OK).send(chat);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

router.get('/', async (req, res) => {
    const {limit, offset} = req.query;
    try {
        const chats = await DBRPC.sendCommand('chats:getAll', [
            limit,
            offset,
        ]);
        res.status(httpStatus.OK).send(chats);
    } catch (err) {
        res.status(httpStatus.BAD_REQUEST).send(err.message);
    }
});

export default router;
