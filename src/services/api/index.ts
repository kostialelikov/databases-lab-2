import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import router from './routes';
import {api, rpc} from '../../../config';
import {Client} from '../../helpers/rpc-client';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cors());

app.use('/api', router);

export let DBRPC: Client = null;

Client.build(rpc.DB).then(client => {
    DBRPC = client;
    app.listen(api.PORT);
});



