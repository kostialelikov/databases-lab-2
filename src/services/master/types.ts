import {ChildProcess} from 'child_process';

export type ServiceName = 'db' | 'master' | 'api';

export interface IServicesList {
    DB: ChildProcess,
    API: ChildProcess
}

export const services = ['db', 'master', 'api'];
