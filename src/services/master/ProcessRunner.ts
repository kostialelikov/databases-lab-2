import {ChildProcess, exec} from 'child_process';
import running from 'is-running';
import * as Types from './types';
import {logger} from './logger';

export const Services: Types.IServicesList = {
    DB: undefined,
    API: undefined
};


export function startAll() {
    run('db');
    run('api');
}

export function run(service: Types.ServiceName) {
    Services[service.toUpperCase()] = exec(startCommand(service), async err => {
        if (err) {
            logger.error('master', err.message);
        }
    });
    setListeners(service);
}

export function setListeners(service: Types.ServiceName) {
    const child: ChildProcess = getProcess(service);
    child.stdout.removeAllListeners();
    child.stderr.removeAllListeners();
    child.stdout.on('data', chunk => logger.info(service, chunk));
    child.stdout.on('error', err => logger.info(service, err.message));
    child.stderr.on('data', chunk => logger.error(service, chunk));
    child.stderr.on('error', err => logger.error(service, err.message));
    child.on('disconnect', () => {
        logger.warn(service, 'Child was disconnected. Reboot attempt.');
        reboot(service);
    });
    child.on('exit', (code, signal) => {
        logger.warn(service, `Child has been exit with code ${code}: ${signal}. Reboot attempt.`);
        reboot(service);
    })
}

export function killAll() {
    Services.DB.kill();
}

export function reboot(service: Types.ServiceName) {
    if (service === 'master') return;
    const child: ChildProcess = getProcess(service);
    if (child && running(child.pid))
        child.kill();
    run(service);
}



function startCommand(service: Types.ServiceName) {
    return `node ${process.cwd()}/dist/src/services/${service}/.`
}

function getProcess(service: Types.ServiceName) {
    switch (service) {
        case "db": return Services.DB;
        case "api": return Services.API;
    }
}
