import {Logger} from 'log4js';

export enum LogFiles {
    ERROR = 'error.log',
    APPLICATION = 'application.log'
}

export const LOGS_DIRECTORY = `${process.cwd()}/logs`;

export interface ServiceLogger {
    error: Logger,
    application: Logger
}
