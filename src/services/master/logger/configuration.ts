import {resolve} from 'path';
import * as Types from './types'

const errorPath = resolve(Types.LOGS_DIRECTORY, Types.LogFiles.ERROR);
const applicationPath = resolve(Types.LOGS_DIRECTORY, Types.LogFiles.APPLICATION);

const layout = {
    type: 'pattern',
    pattern: '%d %p %X{service} %m%n'
};

export const log4jsConfig = {
    appenders: {
        out: {
            type: 'stdout',
            layout
        },
        error: {
            type: 'file',
            filename: errorPath,
            layout
        },
        application: {
            type: 'file',
            filename: applicationPath,
            layout
        }
    },
    categories: {
        default: {
            appenders: ['application', 'error'],
            level: 'all'
        },
        error: {
            appenders: ['error'],
            level: 'error'
        },
        application: {
            appenders: ['application'],
            level: 'info'
        },
        errorDebug: {
            appenders: ['out', 'error'],
            level: 'error'
        },
        applicationDebug: {
            appenders: ['out', 'application'],
            level: 'info'
        }
    }
};
