import log4js from 'log4js';
import {log4jsConfig} from "./configuration";
import * as Types from './types';
import {existsSync, mkdirSync, writeFileSync} from 'fs';
import {resolve} from 'path';
import {Logger} from './logger';

if (!existsSync(resolve(Types.LOGS_DIRECTORY))) {
    mkdirSync(resolve(Types.LOGS_DIRECTORY));
}

if (!existsSync(resolve(Types.LOGS_DIRECTORY, Types.LogFiles.ERROR))) {
    writeFileSync(resolve(Types.LOGS_DIRECTORY, Types.LogFiles.ERROR), {flag: 'wx'});
}

if (!existsSync(resolve(Types.LOGS_DIRECTORY, Types.LogFiles.APPLICATION))) {
    writeFileSync(resolve(Types.LOGS_DIRECTORY, Types.LogFiles.APPLICATION), {flag: 'wx'});
}

log4js.configure(log4jsConfig);

export const logger = new Logger();

