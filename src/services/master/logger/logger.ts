import log4js from 'log4js';
import {ServiceLogger} from './types';
import {ServiceName, services} from "../types";

type loggerEntity = { [service in ServiceName]: ServiceLogger; };

export class Logger {
    private loggers: loggerEntity = {} as loggerEntity;

    constructor() {
        services.forEach(service => {
            const errorLogger = log4js.getLogger(process.env.NODE_ENV === 'development' ? 'errorDebug' : 'error');
            const applicationLogger = log4js.getLogger(process.env.NODE_ENV === 'development' ? 'applicationDebug' : 'application');
            errorLogger.addContext('service', service);
            applicationLogger.addContext('service', service);
            this.loggers[service] = {
                error: errorLogger,
                application: applicationLogger
            };
        });
    }

    public info(service: ServiceName, message: string, ...args: any[]) {
        const logger = this.loggers[service].application;
        logger.info(message, ...args);
    }

    public warn(service: ServiceName, message: string, ...args: any[]) {
        const logger = this.loggers[service].application;
        logger.warn(message, ...args);
    }

    public error(service: ServiceName, message: string | Error, ...args: any[]) {
        const logger = this.loggers[service].error;
        logger.error(message.toString(), ...args);
    }

    public fatal(service: ServiceName, message: string | Error, ...args: any[]) {
        const logger = this.loggers[service].error;
        logger.fatal(message.toString(), ...args);
    }

}
