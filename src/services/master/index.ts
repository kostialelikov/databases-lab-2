import Bluebird from 'bluebird';
import {logger} from './logger';
import {startAll, killAll} from './ProcessRunner';

process.title = 'master';

Bluebird.config({
    cancellation: true
});

global.Promise = Bluebird;

async function main() {
    try {
        startAll();
        process.on('exit', killAll);
        logger.info('master', 'Ready');
    } catch (err) {
        logger.error('master', err.message);
        process.exit(-1);
    }
}

main();

