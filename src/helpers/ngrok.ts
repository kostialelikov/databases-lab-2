const http = require('http');

const options = {
    hostname: '127.0.0.1',
    port: 4040,
    path: '/api/tunnels',
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
    }
};

export const getPublicUrl = (port: string | number) => {
    return new Promise((resolve, reject) => {
        const req = http.request(options, (res: any) => {
            res.setEncoding('utf8');
            res.on('data', (config: any) => {
                config = JSON.parse(config);
                for (let i in config.tunnels) {
                    const current = config.tunnels[i];
                    if (current.proto === 'https' && current.config.addr.split(':').pop() === port.toString()) {
                        resolve(current.public_url);
                    }
                }
            });
        });

        req.on('error', (e: any) => {
            return reject(e.message);
        });

        req.end();
    });
};
