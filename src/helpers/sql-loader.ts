import {QueryFile, utils} from 'pg-promise';
import {PathLike} from 'fs';

export function load(dir: PathLike): any {
    return utils.enumSql(dir.toString(), {recursive: true}, file => {
        return new QueryFile(file, {
            minify: true,
            params: {
                schema: 'public'
            }
        });
    });
}
