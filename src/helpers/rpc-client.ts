import amqplib from 'amqplib';
import {AMQPRPCClient} from '@elastic.io/amqp-rpc';
import {rabbit} from '../../config';

export class Client {
    private readonly queue: string;
    private readonly connectionParams: amqplib.Options.Connect;
    private connection: amqplib.Connection;
    private client: AMQPRPCClient;
    private started: boolean;

    constructor(queue: string) {
        this.started = false;
        this.queue = queue;
        this.connectionParams = {
            protocol: 'amqp',
            hostname: rabbit.HOST,
            port: rabbit.PORT
        }
    }

    public async configureClientAndStart() {
        if (!this.started) {
            this.connection = await amqplib.connect(this.connectionParams);
            this.connection.on('error', (error) => {
                //@TODO redirect error to master
            });
            this.client = new AMQPRPCClient(this.connection, {
                requestsQueue: this.queue
            });
            this.started = true;
            await this.client.start();
        }
    }

    public sendCommand(command: string, args?: any[]) {
        return new Promise<any>(async (resolve, reject) => {
            try {
                const response = await this.client.sendCommand(command, args);
                resolve(response);
            } catch (e) {
                reject(e);
            }
        })
    }

    public static async build(queue: string) {
        const client = new Client(queue);
        await client.configureClientAndStart();
        return client;
    }
}
