import {utils} from 'pg-promise';

export function camelizeColumnsNames(data) {
    const firstRow = data[0];
    const camelObj = {};
    if (firstRow) {
        Object.keys(firstRow).forEach((prop) => {
            const camel = utils.camelize(prop);
            if (camel !== prop) {
                camelObj[prop] = camel;
            }
        });
        for (let i = 0; i < data.length; i++) {
            const row = data[i];
            Object.keys(camelObj).forEach((key) => {
                row[camelObj[key]] = row[key];
                delete row[key];
            });
        }
    }
}
