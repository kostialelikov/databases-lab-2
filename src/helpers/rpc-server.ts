import amqplib from 'amqplib';
import {AMQPRPCServer} from '@elastic.io/amqp-rpc';
import {rabbit} from '../../config';

export interface IConfigurationServer {
    configureServer(): void;
}

export class RPCServer {
    private readonly queue: string;
    private readonly connectionParams: amqplib.Options.Connect;
    private connection: amqplib.Connection;
    protected server: AMQPRPCServer;

    constructor(queue: string) {
        this.queue = queue;
        this.connectionParams = {
            protocol: 'amqp',
            hostname: rabbit.HOST,
            port: rabbit.PORT
        }
    }

    public async initialSetup() {
        const connectionForCreating = await amqplib.connect(this.connectionParams);
        const channel = await connectionForCreating.createChannel();
        await channel.assertQueue(this.queue);
        await connectionForCreating.close();

        this.connection = await amqplib.connect(this.connectionParams);

        this.connection.on('error', (err) => {
            //@TODO redirect error to master
        });

        this.server = new AMQPRPCServer(this.connection, {
            requestsQueue: this.queue
        });
    }

    public async listen() {
        await this.server.start();
    }
}
