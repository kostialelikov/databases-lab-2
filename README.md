# Databases Lab 2

# How to run

* First install node modules
```shell script
npm install
```
* Start ngrok (Needed for CLI)
```shell script
ngrok http 3000
```
* Run services
```shell script
npm run start
```
* Run CLI
```shell script
npm run startcli
```
